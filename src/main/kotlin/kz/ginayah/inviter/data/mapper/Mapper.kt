package kz.ginayah.inviter.data.mapper

interface Mapper<E, D> {

    fun toEntity(dto: D): E

    fun toDto(entity: E): D

    fun toDtoList(entityList: List<E>): List<D>

    fun toEntityList(dtoList: List<D>): List<E>
}
