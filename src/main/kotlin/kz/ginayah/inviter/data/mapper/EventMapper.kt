package kz.ginayah.inviter.data.mapper

import kz.ginayah.inviter.data.dto.EventDto
import kz.ginayah.inviter.data.dto.ImageDto
import kz.ginayah.inviter.data.entity.EventEntity
import kz.ginayah.inviter.data.repository.EventOwnerRepository
import kz.ginayah.inviter.data.repository.ImageRepository
import org.springframework.stereotype.Component
import java.time.LocalDate

/**
 * @author aibek.kokanbekov
 */

@Component
class EventMapper(
    private val imageRepository: ImageRepository,
    private val eventOwnerRepository: EventOwnerRepository
) : AbstractMapper<EventEntity, EventDto>(
    EventEntity::class.java, EventDto::class.java
) {
    override fun toDto(entity: EventEntity): EventDto {
        return super.toDto(entity).apply {
            this.ownerIds = entity.owners?.map { it.getId()!! }
            this.images = entity.images?.map {
                ImageDto(it.getId(), it.file, entity.getId()!!)
            }
            this.eventDate = LocalDate.now()
        }
    }
    
    override fun toEntity(dto: EventDto): EventEntity {
        return super.toEntity(dto).apply {
            this.owners = dto.ownerIds?.let { eventOwnerRepository.findAllById(it) }
            this.images = dto.images?.map {
                it.id
            }?.let { imageRepository.findAllById(it) }
            this.eventDate = LocalDate.now()
        }
    }
}

