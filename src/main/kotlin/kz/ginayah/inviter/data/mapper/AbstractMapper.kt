package kz.ginayah.inviter.data.mapper

import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired

abstract class AbstractMapper<E, D>
    (private val entityClass: Class<E>, private val dtoClass: Class<D>) : Mapper<E, D> {
    @Autowired
    internal lateinit var mapper: ModelMapper

    override fun toDto(entity: E): D =
        entity.let { mapper.map(it, dtoClass) }

    override fun toEntity(dto: D): E =
        dto.let { mapper.map(it, entityClass) }

    override fun toDtoList(entityList: List<E>): List<D> =
        entityList.map { toDto(it) }

    override fun toEntityList(dtoList: List<D>): List<E> =
        dtoList.map { toEntity(it) }

    fun toDto(entity: E, additionalManipulations: (entity: E, dto: D) -> Unit) =
        toDto(entity).also { additionalManipulations(entity, it) }

    fun toDtoList(entityList: List<E>, additionalManipulations: (entity: E, dto: D) -> Unit): List<D> =
        entityList.map { toDto(it, additionalManipulations) }
}
