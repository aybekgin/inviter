package kz.ginayah.inviter.data.mapper

import kz.ginayah.inviter.data.dto.EventOwnerDto
import kz.ginayah.inviter.data.entity.EventOwnerEntity
import kz.ginayah.inviter.data.repository.EventRepository
import org.springframework.stereotype.Component

@Component
class EventOwnerMapper(
    private val eventRepository: EventRepository,
) : AbstractMapper<EventOwnerEntity, EventOwnerDto>(
    EventOwnerEntity::class.java, EventOwnerDto::class.java
) {
    override fun toDto(entity: EventOwnerEntity): EventOwnerDto {
        return super.toDto(entity).apply {
            this.eventId = entity.getId()!!
        }
    }

    override fun toEntity(dto: EventOwnerDto): EventOwnerEntity {
        return super.toEntity(dto).apply {
            this.eventId = eventRepository.getById(dto.eventId)
        }
    }
}
