package kz.ginayah.inviter.data.service

import kz.ginayah.inviter.data.dto.EventDto
import kz.ginayah.inviter.data.entity.EventEntity

/**
 * @author aibek.kokanbekov
 */

interface EventService {
    fun save(eventDto: EventDto): EventEntity
    fun find(): EventDto
}
