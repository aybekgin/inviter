package kz.ginayah.inviter.data.service

import kz.ginayah.inviter.data.dto.EventDto
import kz.ginayah.inviter.data.entity.EventEntity
import kz.ginayah.inviter.data.mapper.EventMapper
import kz.ginayah.inviter.data.repository.EventOwnerRepository
import kz.ginayah.inviter.data.repository.EventRepository
import org.springframework.stereotype.Service

@Service
class EventServiceImpl(
    private val eventRepository: EventRepository,
    private val eventOwnerRepository: EventOwnerRepository,
    val eventMapper: EventMapper
) : EventService {
    override fun save(eventDto: EventDto): EventEntity {
        return eventRepository.save(eventMapper.toEntity(eventDto))
    }
    
    override fun find(): EventDto {
        return eventRepository.findAll().last().let { eventMapper.toDto(it) }
    }
}