package kz.ginayah.inviter.data.entity

import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "image")
class ImageEntity(
    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    val file: ByteArray,
    @ManyToOne
    @JoinColumn(name = "event_id")
    var eventId: EventEntity
) : BaseEntity()
