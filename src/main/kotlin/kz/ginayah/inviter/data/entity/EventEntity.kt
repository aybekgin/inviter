package kz.ginayah.inviter.data.entity

import org.hibernate.annotations.Type
import java.time.LocalDate
import javax.persistence.*

/**
 * @author aibek.kokanbekov
 */

@Entity
@Table(name = "event")
class EventEntity(
    val title: String,
    val eventName: String,
    var eventDate: LocalDate,
    val eventTime: String,
    val eventMainTitle: String,
    val locationName: String,
    val locationAddress: String,
    val eventSmallTitle: String,
    val inviteMessage: String,
    val eventOwnersTitle: String,
    @OneToMany(mappedBy = "eventId", cascade = [CascadeType.ALL], orphanRemoval = true)
    var owners: List<EventOwnerEntity>?,
    @OneToMany(mappedBy = "eventId", cascade = [CascadeType.ALL], orphanRemoval = true)
    var images: List<ImageEntity>?,
    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    val mainBackground: ByteArray?,
    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    val invitationBackground: ByteArray?,
    val imagesTitle: String,
    val groom: String,
    val bride: String
): BaseEntity()