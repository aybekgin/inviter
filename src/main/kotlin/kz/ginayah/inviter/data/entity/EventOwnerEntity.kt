package kz.ginayah.inviter.data.entity

import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

/**
 * @author aibek.kokanbekov
 */

@Entity
@Table(name = "event_owner")
class EventOwnerEntity(
    val name: String,
    val position: String,
    val description: String,
    @ManyToOne
    @JoinColumn(name = "event_id")
    var eventId: EventEntity
) : BaseEntity()