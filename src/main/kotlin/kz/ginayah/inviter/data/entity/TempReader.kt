package kz.ginayah.inviter.data.entity

import java.io.FileWriter
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths

fun main() {
    try {
        val lines = Files.lines(Paths.get("/Users/aibek.kokanbekov/inviter/src/main/resources/1.txt"))
        val myWriter = arrayOf<FileWriter?>(null)
        myWriter[0] = createFileWriter("file.txt")
        lines.forEach { s: String ->
            val field = s.substringBefore("-")
            val text = s.substringAfter("-")
            val v = """
        <div class="form-group row">
            <label for="$field" class="col-sm-2 col-form-label">$text</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="$field" id="$field" placeholder="$text">
            </div>
        </div>
        
            """.trimIndent()
            write(myWriter[0], v)
        }
        close(myWriter[0])
        println("Entry class closed")
    } catch (exc: IOException) {
        exc.printStackTrace()
    }
}

private fun close(fileWriter: FileWriter?) {
    fileWriter!!.close()
}

private fun write(fileWriter: FileWriter?, s2: String) {
    fileWriter!!.write(s2)
}

private fun createFileWriter(fileName: String): FileWriter {
    return FileWriter(fileName)
}