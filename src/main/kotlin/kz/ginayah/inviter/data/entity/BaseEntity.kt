package kz.ginayah.inviter.data.entity

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import java.io.Serializable
import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import org.hibernate.annotations.Generated
import org.hibernate.annotations.GenerationTime
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.data.util.ProxyUtils

/**
 * TODO if entity extends this class batch updates and inserts will not work,
 * because of the Generated annotations on createdAt
 **/
@MappedSuperclass
@TypeDefs(
    TypeDef(name = PG_ENUM, typeClass = PostgreSQLEnumType::class),
    TypeDef(name = PG_JSONB, typeClass = JsonBinaryType::class)
)
abstract class BaseEntity : Serializable {

    companion object {
        private const val serialVersionUID = -7854L
    }

    @Id
    @GeneratedValue
    private var id: UUID? = null

    @Generated(value = GenerationTime.ALWAYS)
    var createdAt: LocalDateTime? = null

    @UpdateTimestamp
    var updatedAt: LocalDateTime? = null

    fun getId(): UUID? {
        return id
    }

    fun setId(id: UUID?) {
        this.id = id
    }

    override fun equals(other: Any?): Boolean {
        other ?: return false

        if (this === other) return true

        if (javaClass != ProxyUtils.getUserClass(other)) return false

        other as BaseEntity

        return if (null == this.getId()) false else this.getId() == other.getId()
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

    override fun toString(): String {
        return "BaseEntity(id=$id, createdAt=$createdAt)"
    }
}
