package kz.ginayah.inviter.data.repository

import kz.ginayah.inviter.data.entity.ImageEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*


/**
 * @author aibek.kokanbekov
 */

interface ImageRepository : JpaRepository<ImageEntity, UUID> {
}
