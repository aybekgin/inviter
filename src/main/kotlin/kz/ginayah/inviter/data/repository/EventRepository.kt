package kz.ginayah.inviter.data.repository

import kz.ginayah.inviter.data.entity.EventEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*


/**
 * @author aibek.kokanbekov
 */

interface EventRepository : JpaRepository<EventEntity, UUID> {
}
