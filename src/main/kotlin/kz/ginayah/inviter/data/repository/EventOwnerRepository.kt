package kz.ginayah.inviter.data.repository

import kz.ginayah.inviter.data.entity.EventOwnerEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*


/**
 * @author aibek.kokanbekov
 */

interface EventOwnerRepository : JpaRepository<EventOwnerEntity, UUID> {
}
