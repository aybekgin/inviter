package kz.ginayah.inviter.data.dto

import java.util.*

/**
 * @author aibek.kokanbekov
 */

class ImageDto(
    val id: UUID? = null,
    val file: ByteArray,
    val eventId: UUID
) : AbstractDto()
