package kz.ginayah.inviter.data.dto

import java.lang.annotation.Inherited

@Inherited
@Target(AnnotationTarget.CLASS)
annotation class NoArgs
