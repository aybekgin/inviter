package kz.ginayah.inviter.data.dto

import java.util.*

class EventOwnerDto(
    val id: UUID? = null,
    val name: String,
    val position: String,
    val description: String,
    var eventId: UUID
) : AbstractDto()
