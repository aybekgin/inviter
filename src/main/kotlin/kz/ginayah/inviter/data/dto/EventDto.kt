package kz.ginayah.inviter.data.dto

import java.time.LocalDate
import java.util.*


/**
 * @author aibek.kokanbekov
 */

class EventDto(
    val id: UUID? = null,
    val title: String?,
    val eventName: String?,
    var eventDate: LocalDate = LocalDate.now(),
    val eventTime: String?,
    val eventMainTitle: String?,
    val locationName: String?,
    val locationAddress: String?,
    val eventSmallTitle: String?,
    val inviteMessage: String?,
    val eventOwnersTitle: String?,
    var ownerIds: List<UUID>?,
    var images: List<ImageDto>?,
    val mainBackground: ByteArray?,
    val invitationBackground: ByteArray?,
    val imagesTitle: String?,
    val groom: String?,
    val bride: String?
) : AbstractDto()
