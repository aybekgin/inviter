package kz.ginayah.inviter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class InviterApplication

fun main(args: Array<String>) {
	runApplication<InviterApplication>(*args)
}
