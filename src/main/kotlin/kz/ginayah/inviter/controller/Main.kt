package kz.ginayah.inviter.controller

import kz.ginayah.inviter.data.dto.EventDto
import kz.ginayah.inviter.data.service.EventService
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import javax.servlet.http.HttpServletRequest

/**
 * @author aibek.kokanbekov
 */

@Controller("")
class Main(
    private val eventService: EventService
) {
    @GetMapping
    fun index(model: Model, request: HttpServletRequest): String {
        val d: EventDto = eventService.find()
        model.addAttribute("event", d)
        return "/index"
    }
    
    @GetMapping("/events/create-form")
    fun createForm(): String {
        return "/createForm"
    }
    
    @PostMapping("/events/create")
    fun createProductInfo(@ModelAttribute eventDto: EventDto): ResponseEntity<String> {
        val d = eventService.save(eventDto)
        return ResponseEntity.ok("OK")
    }
}
