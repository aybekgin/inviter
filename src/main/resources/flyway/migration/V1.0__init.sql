CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE event
(
    id                    uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    title                 varchar(100)                   ,
    event_name            varchar(150)                   ,
    event_date            date                           ,
    event_time            varchar(5)                     ,
    event_main_title      varchar(100)                   ,
    location_name         varchar(250)                   ,
    location_address      varchar(300)                   ,
    event_small_title     varchar(150)                   ,
    invite_message        varchar(200)                   ,
    event_owners_title    varchar(100)                   ,
    main_background       bytea                          ,
    invitation_background bytea                          ,
    images_title          varchar(100)                   ,
    groom                 varchar(50)                    ,
    bride                 varchar(50)                    ,
    created_at            timestamp        default now() ,
    updated_at            timestamp
);

CREATE TABLE event_owner
(
    id          uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name        varchar(100)                   ,
    position    varchar(100)                   ,
    description varchar(100)                   ,
    event_id    uuid references event (id)     ,
    created_at  timestamp        default now() ,
    updated_at  timestamp
);

CREATE TABLE image
(
    id         uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    file       bytea                          ,
    event_id   uuid references event (id)     ,
    created_at timestamp        default now() ,
    updated_at timestamp
);
